import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FilmsServiceService } from '../services/films-service.service';
import { Film } from '../models/Film';
import { Router } from '@angular/router';

@Component({
  selector: 'app-films-list',
  templateUrl: './films-list.component.html',
  styleUrls: ['./films-list.component.css']
})
export class FilmsListComponent implements OnInit {

  shortSynopsis: boolean;
  films: Film[];
  filmSubscription: Subscription;
  stringsToShow: string[];

  constructor(private filmsService: FilmsServiceService, private router: Router) { }

  ngOnInit() {
    this.stringsToShow = [];
    this.filmSubscription = this.filmsService.filmSubject.subscribe(
      (films) => {
        this.films = films;
        console.log(this.films.length);
        this.films.forEach (
          element => {
            this.stringsToShow.push(element.getShortSynopsis());
            console.log("ELEMENT : " + this.stringsToShow[0]);
          }
        );
      }
    )
    this.filmsService.emitFilms();
    this.films.forEach (
      element => {
        this.stringsToShow.push(element.getShortSynopsis());
      }
    );
  }

  onDetails(i: number){
    this.router.navigate(['/films', 'view', i]);
  }

}
