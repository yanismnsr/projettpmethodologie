import { Component, OnInit } from '@angular/core';
import { Film } from '../../models/Film';
import { FilmsServiceService } from '../../services/films-service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-single-film',
  templateUrl: './single-film.component.html',
  styleUrls: ['./single-film.component.css']
})
export class SingleFilmComponent implements OnInit {

  film: Film;

  constructor(private filmsService: FilmsServiceService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.filmsService.getHelloWorld();
    const id = +this.route.snapshot.params['id'];
    if (id >= 0 && id < this.filmsService.films.length) {
      this.film = this.filmsService.getSingleFilm(id);
    }else {
      this.router.navigate(['films']);
    }
  }

}
