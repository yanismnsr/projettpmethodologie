export class Film{

    private name: string;
    private photo: string;
    private synopsis: string;
    private available: boolean;
    private producer: string;
    private genre: string;
    private nationality: string;

    constructor(name: string, photoPath: string, synopsis: string, available: boolean) {
        this.name = name;
        this.photo = photoPath;
        this.synopsis = synopsis;
        this.available = available;
    }

    getName(){
        return this.name;
    }

    getPhotoPath(){
        return this.photo;
    }

    getSynopsis(){
        return this.synopsis;
    }

    getAvailable(){
        return this.available;
    }

    getShortSynopsis() {
        var tabMots = this.synopsis.split(" ");
        if (tabMots.length > 20) {
            return tabMots.slice(0,20).join(" ") + " ...";
        }else {
            return this.synopsis;
        }
    }

}