import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FilmsListComponent } from './films-list/films-list.component';
import { SingleFilmComponent } from './films-list/single-film/single-film.component';
import { FilmFormComponent } from './films-list/film-form/film-form.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule, Routes } from '@angular/router';
import { AlertModule } from 'ngx-bootstrap';
import { FilmsServiceService } from './services/films-service.service';
import { HttpClientModule } from '@angular/common/http';


const appRoutes: Routes = [
  { path: 'films', component: FilmsListComponent },
  { path: 'films/view/:id', component: SingleFilmComponent },
  { path : '', redirectTo: 'films', pathMatch: 'full' },
  { path: '**', redirectTo: 'films' }
];

@NgModule({
  declarations: [
    AppComponent,
    FilmsListComponent,
    SingleFilmComponent,
    FilmFormComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AlertModule.forRoot(),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    FilmsServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
