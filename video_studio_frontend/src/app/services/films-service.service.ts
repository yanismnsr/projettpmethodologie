import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Film } from '../models/Film';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class FilmsServiceService {

  filmSubject = new Subject<Film[]>();
  films: Film[] = new Array<Film>();
  urlApi = "http://localhost:8080/movie";

  constructor(
    private http: HttpClient,
    private router: Router
  ) { 
    this.emitFilms();
  }

  emitFilms(){
    this.getHelloWorld();
    this.filmSubject.next(this.films);
  }

  addFilm(film: Film){
    this.films.push(film);
    this.emitFilms();
  }

  getSingleFilm(id: number){
    return this.films[id];
  }


  getHelloWorld(){
    let tableaufilms: any[] = new Array();
    console.log("je suis dans la fonction de requetes");
    console.log(this.router.url);
    
    /*this.http.get(this.urlApi, {responseType: 'text'}).subscribe(
      (data) => {
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );*/

    const user = {
      "id" : "3",
      "name" : "username",
      "rented films": 6
    }

    this.http.post(this.urlApi, user).subscribe(
      (data) => {
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );

    this.http.get(this.urlApi).subscribe(
      
      (data) => {
        console.log("REPONSE");
        console.log(data);
        console.log("La taille de data : " + data["data"].length);
        tableaufilms = data["data"];
        for(let i = 0; i < tableaufilms.length; i++){
          this.films.push(new Film(tableaufilms[i].name, tableaufilms[i].photo, tableaufilms[i].synopsis, tableaufilms[i].available));
        }
      },
      (error) => {
        console.error(error);
      }
    )

    
    
  }

  

}
