var express = require("express");
var router = express.Router();

var movies = [
    {
        "name": "title",
        "photo": "assets/données/livres.jpg",
        "synopsis" : "synopsis synopsis synopsis synopsis synopsis synopsis synopsis synopsis",
        "available": true
    },
    {
        "name": "Harry Potter",
        "photo": "assets/données/51Pab2aMLHL.jpg",
        "synopsis" : "La quatrième année à l'école de Poudlard est marquée par le Tournoi des trois sorciers. Les participants sont choisis par la fameuse coupe de feu qui est à l'origine d'un scandale. Elle sélectionne Harry Potter alors qu'il n'a pas l'âge légal requis !",
        "available": true
    }
];

router.route("/")
.get(function(req,res){
    res.json({message : "liste des films", data: movies});
})
.post(function(req,res){
    console.log(req.body);
    // movies.push(req.body);
    res.json({message : "Ajouter films", name: req.body.name, annee: req.body.annee, type: req.body.type, available: req.body.available})
})

router.route('/:id',(req,res)=>{
    console.log(req.params.id)
}).put((req,res)=>{
    console.log(req.params)
})
module.exports = router;