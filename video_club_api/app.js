var express = require('express');
var hostname = 'localhost'; 
var port = 8080;

var userRouter = require ("./route/user");
var movieRouter = require ("./route/movie");

var app = express(); 

const cors = require('cors');
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.use("/user", userRouter);
app.use("/movie", movieRouter);






app.get('/', function(req, res){
    res.send('hello world');
});




app.listen(port, hostname, function(){
	console.log("Mon serveur fonctionne sur http://"+ hostname +":"+port); 
});
